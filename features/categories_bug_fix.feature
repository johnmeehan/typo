Feature: Categories Bug Fix
  As a blog administrator
  In order to organise catagories 
  I want create and edit categories

  Background:
    Given the blog is set up
    And I am logged into the admin panel
    When I follow "Categories"
    
    
  Scenario: Link works properly
  	Then I should see "Categories"
  	And I should see "General"
  	
#he categories page
#  should support creating new categories [50 points] (FAILED)
#  should support editing existing categories [50 points] (FAILED)

  Scenario: Successfully Create a Category
    #Given I am on the Categories page
    When I fill in "category_name" with "NewCat"
    When I fill in "category_keywords" with "NewCat"
    When I fill in "category_description" with "NewCat"
    And I press "Save"
    #Then I should be on the Categories page
    Then I should see "NewCat"
    
    
    # when i want to edit a categorie i call:
    # /admin/categories/edit/1      I can see the category "General" permalink "general"
  Scenario: Successfully edit a Category
    #Given I am on the Categories page
    Then I should see "General"
    When I follow "General"
    When I fill in "category_name" with "General-Edit"
    And I press "Save"
    #Then I should be on the Categories page
    Then I should see "General-Edit"
    
    #When I fill in "article_title" with "Foobar"
    #And I fill in "article__body_and_extended_editor" with "Lorem Ipsum"
    #And I press "Publish"
    #Then I should be on the admin content page
    #When I go to the home page
    #Then I should see "Foobar"
   # When I follow "Foobar"
    #Then I should see "Lorem Ipsum"
    
    
 
   
